package net.panic.tests;

import net.panic.ErrorResult;
import net.panic.Expect;
import net.panic.Option;
import net.panic.Panic;
import org.junit.Test;
import org.junit.internal.runners.ErrorReportingRunner;
import org.junit.jupiter.api.AfterEach;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PanicTest {
    @AfterEach
    public void resetPanicHandler(){
        Panic.setPanicHandler(Panic.defaultPanicHandler);
    }

    @Test
    public void expectTest(){
        Expect<Integer, ErrorResult> i = new Expect<>(1);
        i.unwrap();
    }

    @Test
    public void optionTest(){
        Option<Integer> i = new Option<>(1);
        i.unwrap();
    }

    @Test
    public void optionTest2(){
        Option<String> i = new Option<>();
        boolean hasPanicked = false;
        try {
            i.unwrap();
        } catch (Panic.PanicException e){
            hasPanicked = true;
        }
        assertTrue(hasPanicked);
    }

    @Test
    public void panicHandlerTest(){
        Panic.setPanicHandler(msg -> {
            System.err.println("panic: " + msg);
            System.err.println("Stacktrace: ");
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                System.err.println("\t" + ste);
            }
        });
        assertNull(Panic.panic("test"));
    }

    @Test
    public void expectTestP(){
        Expect<Integer, ErrorResult> i = new Expect<>(() -> "Test");
        Panic.setPanicHandler(msg -> {
            System.err.println("panic: " + msg);
            System.err.println("Stacktrace: ");
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                System.err.println("\t" + ste);
            }
        });
        assertNull(i.unwrap());
    }

    @Test
    public void optionTestP(){
        Option<Integer> i = new Option<>();
        Panic.setPanicHandler(msg -> {
            System.err.println("panic: " + msg);
            System.err.println("Stacktrace: ");
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                System.err.println("\t" + ste);
            }
        });
        assertNull(i.unwrap());
    }

    @Test
    public void panicPanicsTest(){
        try{
            Expect<String, ?> expect = new Expect<>(() -> "");
            expect.unwrap();
        }
        catch (RuntimeException e){
            System.out.println("panic");
        }
    }



}
