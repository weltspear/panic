package net.panic;

import org.jetbrains.annotations.NotNull;

import static net.panic.Panic.panic;

public class Expect<T, R extends ErrorResult> {
    private final T some;
    private final R result;

    public Expect(@NotNull R result){
        this.some = null;
        this.result = result;
    }

    public Expect(@NotNull T some){
        this.some = some;
        this.result = null;
    }

    /***
     * On failed unwrap panics.
     */
    @Panics
    public T unwrap() {
        if (some == null){
            return panic("Unable to unwrap null");
        }
        else{
            return some;
        }
    }

    /***
     * Gets ErrorResult
     */
    @Panics
    public R getResult(){
        if (result == null){
            return panic("Unable to get expect_msg");
        }
        else {
            return result;
        }
    }

    public boolean isNone(){
        return some == null;
    }

}
