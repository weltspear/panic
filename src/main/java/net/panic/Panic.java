package net.panic;

public class Panic {
    public static class PanicException extends RuntimeException{
        public PanicException(String message) {
            super(message);
        }
    }

    /***
     * Default panic handler which throws a PanicException.
     */
    public static final PanicHandler defaultPanicHandler = msg -> {
        throw new PanicException("panic");
    };

    private static PanicHandler handler = defaultPanicHandler;

    /***
     * Panics with null return value.
     */
    public static<T> T panic(String msg) {
        handler.panic(msg);
        return null;
    }

    public static void setPanicHandler(PanicHandler h){
        handler = h;
    }
}
