package net.panic;

public interface PanicHandler {
    void panic(String msg);
}
