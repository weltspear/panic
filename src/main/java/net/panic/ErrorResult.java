package net.panic;

public interface ErrorResult {
    String message();
}
