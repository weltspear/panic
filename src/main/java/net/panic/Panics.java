package net.panic;

/***
 * Marks a method as able to call `panic`
 */
public @interface Panics {
}
