package net.panic;

import static net.panic.Panic.panic;

public class Option<T>{
    private final T some;

    public Option(){
        some = null;
    }

    public Option(T some){
        this.some = some;
    }

    public boolean isNone(){
        return some == null;
    }

    @Panics
    public T unwrap(){
        return this.some != null ? this.some: panic("Unable to unwrap null");
    }

}
